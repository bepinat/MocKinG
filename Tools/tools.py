import math
import numpy as np
from astropy.io import fits
from astropy.io import ascii
import os
import sys
import yaml
import logging
from matplotlib import pyplot as plt

logger = logging.getLogger('__mocking__')


def generate_input(fileparam, pathout, filemeth='', verbose=False, method='mpfit',
                   ftol=1e-10, xtol=1e-10, gtol=1e-10,
                   pltstats=False, max_iter=0, n_live_points=50, sampling_efficiency=0.8,
                   evidence_tolerance=0.5, n_iter_before_update=100, null_log_evidence=-1e90,
                   max_modes=100, mode_tolerance=-1e60,
                   vel='vel.fits', errvel='evel.fits', disp='disp.fits', psf='psf.fits',
                   psfx=3, psfz=50, smooth=0, oversample=4, sig=0, slope=0,
                   flux=['flux.fits']):
    '''
    XXX TBD
    '''
    # Deal with method inputs
    try:
        catm = ascii.read(filemeth)[0]
        dicmeth = {}
        try:
            dicmeth['verbose'] = (catm['verbose'] == 'True')
        except:
            dicmeth['verbose'] = verbose
        try:
            dicmeth['method'] = catm['method'].item()
        except:
            dicmeth['method'] = method
    except:
        dicmeth = {'verbose': verbose, 'method': method}
    
    method = dicmeth['method']
    if method == 'mpfit':
        params = {'ftol':ftol, 'xtol':xtol, 'gtol':gtol}
    elif method == 'multinest':
        params = {'pltstats':pltstats, 'max_iter':max_iter, 'n_live_points':n_live_points, 'sampling_efficiency':sampling_efficiency,
                  'evidence_tolerance':evidence_tolerance, 'n_iter_before_update':n_iter_before_update, 'null_log_evidence':null_log_evidence,
                  'max_modes':max_modes, 'mode_tolerance':mode_tolerance}
    
    dicmeth[method] = {}
    for key in params:
        try:
            dicmeth[method][key] = catm[key].item()
        except:
            dicmeth[method][key] = params[key]
    
    # Deal with model input
    catp = ascii.read(fileparam)
    for line in catp:
        name = line['name'].item()
        dictowrite = {'name': name, 'files': {}, 'resol params': {}, 'objects': {}, 'config fit': dicmeth}
        
        # Global files
        filedico = {'vel':vel, 'errvel':errvel, 'disp':disp, 'psf':psf}
        for key in filedico:
            try:
                if line[key] == 'None':
                    dictowrite['files'][key] = None
                else:
                    dictowrite['files'][key] = line[key].item()
            except:
                dictowrite['files'][key] = filedico[key]
        
        # Resolution parameters
        resdico = {'psfx':psfx, 'psfz':psfz, 'smooth':smooth, 'oversample':oversample}
        for key in resdico:
            try:
                if line[key] == 'None':
                    dictowrite['resol params'][key] = None
                else:
                    dictowrite['resol params'][key] = line[key].item()
            except:
                dictowrite['resol params'][key] = resdico[key]
        
        # Deal with objects and components
        obj_list = [s for s in catp.keys() if "obj" in s]  # find objects
        objsuff = ''
        for obj in obj_list:
            objsuff += '_' + obj
            number = int(obj.split('obj')[-1])
            dicobject = {}
            try:
                dicobject['files'] = {'flux':line['flux'].item()}
            except:
                dicobject['files'] = {'flux':flux[number -1]}
            try:
                dicobject['dispersion info'] = {'sig':line['sig'].item(), 'slope':line['slope'].item()}
            except:
                dicobject['dispersion info'] = {'sig':sig, 'slope':slope}
            
            dicobject['params'] = {'xc':{}, 'yc':{}, 'pa':{}, 'inc':{}, 'vs':{}}
            suff_fix = ''
            for key in dicobject['params']:
                val = line[key].item()
                up = line[key + '_up'].item()
                low = line[key + '_low'].item()
                #limits = '[{}, {}]'.format(low, up)
                limits = [low, up]
                fixed = int(line[key + '_fix'].item() > 0)
                dicobject['params'][key]['value'] = val
                dicobject['params'][key]['limits'] = limits
                dicobject['params'][key]['fixed'] = fixed
                dicobject['params'][key]['desc'] = key
                if fixed == 1:
                    suff_fix += key[0]
            if suff_fix != '':
                objsuff += '_' + suff_fix
            
            dicobject['components'] = {}
            comp_list = [s for s in catp.keys() if "comp" in s]  # find components
            for comp in comp_list:
                compsuff = ''
                compn = int(comp.split('comp')[-1])
                compsuff += '_' + line[comp]
                diccomp = {}
                diccomp['model'] = line[comp].item()
                diccomp['params'] = {}
                
                # searching for components parameters
                params_list = list(set([s.split('_')[1] for s in catp.keys() if "c{}".format(compn) in s]))
                suff_fix = ''
                for key in params_list:
                    diccomp['params'][key] = {}
                    keyc = "c{}_{}".format(compn, key)
                    val = line[keyc].item()
                    up = line[keyc + '_up'].item()
                    low = line[keyc + '_low'].item()
                    limits = [low, up]
                    fixed = int(line[keyc + '_fix'].item() > 0)
                    diccomp['params'][key]['value'] = val
                    diccomp['params'][key]['limits'] = limits
                    diccomp['params'][key]['fixed'] = fixed
                    diccomp['params'][key]['desc'] = key
                    if fixed == 1:
                        suff_fix += key[0]
                
                if suff_fix != '':
                    compsuff += '_' + suff_fix
                objsuff += compsuff
                
                dicobject['components'][comp] = diccomp
            
            dictowrite['objects'][obj] = dicobject
        
        fileout = '{}_{}{}.yaml'.format(name, method, objsuff)
        
        outstream = open(pathout + fileout, 'w')
        yaml.dump(dictowrite, outstream, default_flow_style=False)
        outstream.close()
    
    return


def test_config(config):
    """
    Tests configuration file entries and correct with default values for needed keys
    
    :param dictionary config: configuration file structure
    :return dictionary: correct configuration file
    """
    
    config2 = config.copy()
    
    # Test dispersion information
    for obj in config['objects']:
        try:
            disp_info = config['objects'][obj]['dispersion info']
            try:
                sig = disp_info['sig']
            except:
                logger.warning(" Put default values for dispersion information for {}:\n\t\t  sig = 0".format(obj))
                config2['objects'][obj]['dispersion info']['sig'] = 0
            try:
                slope = disp_info['slope']
            except:
                logger.warning(" Put default values for dispersion information for {}:\n\t\t  slope = 0".format(obj))
                config2['objects'][obj]['dispersion info']['slope'] = 0
        except:
            logger.warning(" No dispersion information (dispersion info). \n\t\t  Put default values for dispersion information for {}:\n\t\t  sig = 0, slope = 0".format(obj))
            config2['objects'][obj]['dispersion info'] = {}
            config2['objects'][obj]['dispersion info']['sig'] = 0
            config2['objects'][obj]['dispersion info']['slope'] = 0
    
    # Test resol params
    try:
        resol_params = config['resol params']
    except:
        logger.warning(" No resolution/sampling information (resol params).")
        config2['resol params'] = {}
    try:
        psfx = resol_params['psfx']
    except:
        logger.error(" No spatial PSF (psfx) information!")
    try:
        psfx = resol_params['psfz']
    except:
        logger.error(" No spectral PSF (psfz) information!")
    try:
        beta = resol_params['beta']
    except:
        logger.warning(" No beta (Moffat PSF) parameter:\n\t\t Assuming PSF is Gaussian")
        config2['resol params']['beta'] = None
    try:
        psfx = resol_params['smooth']
    except:
        logger.warning(" No smoothing (smooth) information:\n\t\t Assuming smooth = 0")
        config2['resol params']['smooth'] = 0
    try:
        psfx = resol_params['oversample']
    except:
        logger.warning(" No oversampling (oversample) information:\n\t\t Using default value oversample = 4")
        config2['resol params']['oversample'] = 4
    
    # Test config fit
    try:
        config_fit = config['config fit']
    except:
        logger.warning(" No fit configuration information (config fit).")
        config2['config fit'] = {}
    try:
        verbose = config_fit['verbose']
    except:
        logger.info(" No verbose information (verbose): setting to False by default")
        config2['config fit']['verbose'] = False
    try:
        method = config_fit['method']
    except:
        logger.warning(" No method information (method): using mpfit by default")
        method = 'mpfit'
        config2['config fit']['method'] = method
    try:
        conf_method = config_fit[method]
    except:
        logger.info(" No {0} method information ({0})".format(method))
        config2['config fit'][method] = {}
        
    conf_method = config2['config fit'][method]
    
    if method == 'mpfit':
        params = {'ftol':1e-10, 'xtol':1e-10, 'gtol':1e-10}
    elif method == 'multinest':
        params = {'pltstats':False, 'max_iter':0, 'n_live_points':50, 'sampling_efficiency':0.8,
                            'evidence_tolerance':0.5, 'n_iter_before_update':100, 'null_log_evidence':-1e90,
                            'max_modes':100, 'mode_tolerance':-1e60}
    else:
        logger.warning(" Incorrect method information: {} \n\t\t  Should be either mpfit or multinest".format(method))
        return config2
    
    for param in params:
        try:
            pval = conf_method[param]
        except:
            logger.info(' {0} is put to default: {1}'.format(param, params[param]))
            config2['config fit'][method][param] = params[param]

    return config2


def set_params(objects):
    """
    Set parameters for a single object with possibly multiple components
    
    :param dictionary objects: 'objects' entry of the configuration file
    :return dictionary: for each object, contains the names, initial guesses, constraints of the parameters, corresponding model and component.
    """
    
    params = {}
    for objn in objects:
        obj = objects[objn]
        params[objn] = {}
        params[objn]['geometrical'] = {}
        for param in obj['params']:
            params[objn]['geometrical'][param] = obj['params'][param].copy()
            params[objn]['geometrical'][param]['component'] = 'geometrical'
            params[objn]['geometrical'][param]['model'] = 'geometrical'
        for compn in obj['components']:
            comp = obj['components'][compn]
            params[objn][compn] = {}
            for param in comp['params']:
                #params[objn][comp['model']][param] = comp['params'][param].copy()
                #params[objn][comp['model']][param]['component'] = compn
                params[objn][compn][param] = comp['params'][param].copy()
                params[objn][compn][param]['component'] = compn
                params[objn][compn][param]['model'] = comp['model']
    return params


def set_files(filesg, objects):
    """
    Set files list from global file entry and objects entry
    
    :param dictionary filesg: 'files' entry of the configuration file
    :param dictionary objects: 'objects' entry of the configuration file
    :return dictionary: files entry for both global and individual objects
    """
    
    files = {}
    files['disp'] = None
    files['psf'] = None
    for fn in filesg:
        files[fn] = filesg[fn]
    files['flux'] = {}
    for objn in objects:
        obj = objects[objn]
        files['flux'][objn] = obj['files']['flux']
    return files


def set_models(objects):
    """
    Set files list from global file entry and objects entry
    
    :param dictionary objects: 'objects' entry of the configuration file
    :return dictionary: for each object, a list of the names of the model corresponding to each component
    """
    
    models = {}
    models['objects'] = {}
    models['components'] = {}
    for objn in objects:
        obj = objects[objn]
        models['objects'][objn] = []
        models['components'][objn] = []
        for compn in obj['components']:
            models['objects'][objn].append(obj['components'][compn]['model'])
            models['components'][objn].append(compn)
    return models


def lowres_indices_highres_frame(im_shape, oversamp=1):
    """
    Compute low resolution indices in the high resolution frame
    
    :param ndarray im_shape: shape of the high resolution frame (pixels)
    :param int oversamp: oversampling factor
    :return ndarray, ndarray: y, x
    """
    y, x = np.indices(im_shape)
    y = (y - (oversamp/2 - 0.5)) / oversamp
    x = (x - (oversamp/2 - 0.5)) / oversamp
    return y, x


def sky_coord_to_galactic(x, y, xc, yc, pa, inc):
    """
    Converts position from Sky coordinates to Galactic coordinates

    :param float x: low resolution x-indices in the high resolution frame (pixel)
    :param float y: low resolution x-indices in the high resolution frame (pixel)
    :param float xc: x-index of the center in low resolution (pixel)
    :param float yc: y-index of the center in low resolution (pixel)
    :param float pa: position angle of the major axis degree
    :param float inc: inclination of the disk in degree
    :return ndarray, ndarray: r, theta
    """
    den = (y - yc) * math.cos(math.radians(pa)) - (x - xc) * math.sin(math.radians(pa))
    num = - (x - xc) * math.cos(math.radians(pa)) - (y - yc) * math.sin(math.radians(pa))
    r = (den ** 2 + (num / math.cos(math.radians(inc))) ** 2) ** 0.5
    sg = np.sign(den)  # sign
    num[np.where(den != 0)] /= den[np.where(den != 0)]  # to avoid a NaN at the center
    den2 = math.cos(math.radians(inc)) ** 2 + num ** 2
    ctheta = sg * (math.cos(math.radians(inc)) ** 2 / den2) ** 0.5  # azimuth in galaxy plane
    
    return r, ctheta


#def sky_coord_to_galactic(xc, yc, pa, inc, im_size=None, oversamp=1):
    #"""
    #Converts position from Sky coordinates to Galactic coordinates

    #:param float xc: position of the center in arcsec
    #:param float yc: position of the center in arcsec
    #:param float pa: position angle of the major axis degree
    #:param float inc: inclination of the disk in degree
    #:param ndarray im_size: maximum radius of the scene (arcsec),
                          #im_size should be larger than the slit length + seeing (Default im_size=100)
    #:param int oversamp: oversampling factor
    #:return list[ndarray, ndarray]: [r, theta]
    #"""
    ## XXX il faudrait faire ces calculs dans la classe, éventuellement si l'oversampling doit être modifié, mais pas besoin de refaire ces opérations à chaque itération
    #y, x = np.indices(im_size)
    #y = (y - (oversamp/2 - 0.5)) / oversamp
    #x = (x - (oversamp/2 - 0.5)) / oversamp
    
    #den = (y - yc) * math.cos(math.radians(pa)) - (x - xc) * math.sin(math.radians(pa))
    #num = - (x - xc) * math.cos(math.radians(pa)) - (y - yc) * math.sin(math.radians(pa))
    #r = (den ** 2 + (num / math.cos(math.radians(inc))) ** 2) ** 0.5
    #sg = np.sign(den)  # sign
    #num[np.where(den != 0)] /= den[np.where(den != 0)]  # to avoid a NaN at the center   XXX ???
    #den2 = math.cos(math.radians(inc)) ** 2 + num ** 2
    ##tpsi = num * 1.
    ##tpsi[np.where(den != 0)] /= den[np.where(den != 0)]  # to avoid a NaN at the center
    ##den2 = math.cos(math.radians(inc)) ** 2 + tpsi ** 2
    #ctheta = sg * (math.cos(math.radians(inc)) ** 2 / den2) ** 0.5  # azimuth in galaxy plane
    
    #return [r, ctheta]


def rebin_data(data, factor):
    """
    Rebins an image.

    example: For rebin an image from 240x240 to 60x60 pixels, factor=5

    :param ndarray data: array to rebin
    :param int factor: rebin factor
    """
    if data.ndim == 2:
        data2 = data.reshape(int(data.shape[0] / factor), factor, int(data.shape[1] / factor), factor)
        return data2.mean(1).mean(2)

    if data.ndim == 3:
        data2 = data.reshape(data.shape[0], int(data.shape[1] / factor), factor, int(data.shape[2] / factor), factor)
        return data2.mean(2).mean(3)


def write_fits(data, filename, config, model, results, mask=None):
    """
        write data in fits file with model's parameters

    :param ndarray data: data to write in fits file
    :param str filename: name of the fits file (with path)
    :param dict config: config file
    :param dict results: dictionary of the results
    :param ndarray[bool] mask: boolean mask
    :return:
    """
    if mask is not None:
        data[np.logical_not(mask)] = float('nan')

    hdu = fits.PrimaryHDU(data=data)

    for el in results['results']:
        for key in el:
            try:
                hdu.header.append((key, el[key]['value'], el[key]['desc']))
            except KeyError as k:
                hdu.header.append((key, el[key]['value']))
                logger.exception("key 'desc' not found, parameter '{}' written without description".format(key), exc_info=k)

    hdulist = fits.HDUList(hdu)
    hdulist.writeto(filename + '.fits', checksum=True, overwrite=True)


def search_file(path, filename):
    """
    Searches a file in a directory and all its subdirectories. Return the path of the file

    :param str path: path where to search
    :param str filename: name of the file to search (with its extension)
    :return str: the relative path where the file is
    """
    try:
        for root, dirs, files in os.walk(path):
            if filename in files:
                if root == '':
                    logger.debug("path is '' :"+filename)
                    return filename
                elif root == path:
                    toreturn = root+filename
                    logger.debug(toreturn)
                    return toreturn
                else:
                    toreturn = root+'/'+filename
                    logger.debug(toreturn)
                    return toreturn
        logger.error("File {} not found in directory {} and its subdirectories".format(filename, path))
        sys.exit()
    except FileNotFoundError as F:
        logger.exception('No such file or directory {}'.format(path), exc_info=F)
        sys.exit()


def make_dir(path, config):
    """
    Creates the directory where results will be written
    The name of the directory depends on fixed parameters

    :param path: path where fits files are
    :param dict config: YAML config dictionary
    :return str: the path
    """

    if path == '.':
        path = './'

    dirname = config['name'] + '_' + config['config fit']['method']

    for objn in config['objects']:
        dirname += '_' + objn
        obj = config['objects'][objn]
        suffix = ''
        for param in obj['params']:
            if obj['params'][param]['fixed'] == 1:
                suffix += param[0]
        if suffix != '':
            dirname += '_' + suffix
        
        for compn in obj['components']:
            comp = obj['components'][compn]
            dirname += '_' + comp['model']
            suffix = ''
            for param in comp['params']:
                if comp['params'][param]['fixed'] == 1:
                    suffix += param[0]
            if suffix != '':
                dirname += '_' + suffix
    
    if os.path.isdir(path+dirname) is False:
        logger.info("\ncreate directory {}".format(dirname))
        os.makedirs(path+dirname)
    toreturn = path+dirname
    logger.debug('makedir: {}'.format(toreturn))
    return toreturn


def write_yaml(path, params, galname, method, whd):
    """
    Sets up the stream and writes the YAML file which contains the results

    :param str path: path where to write the YAML file
    :param list params: list of the bestfit parameters
    :param str galname: the name of the galaxy
    :param str whd: suffix when a high resolution map is used
    :return:
    """

    outstream = open(path+'/results'+whd+'.yaml', 'w')

    dictowrite = {'name': galname, 'results': ''}

    sub_dict = {}
    
    for el in params['results']:
        for key in el:
            #sub_dict.update({key: {'value': float(params['results'][key]['value']), 'error': float(params['results'][key]['error'])}})
            sub_dict.update({key+'_'+el[key]['component']: {'value': float(el[key]['value']), 'error': float(el[key]['error']), 'component': el[key]['component'], 'model': el[key]['model'], 'desc': el[key]['desc']}})

    dictowrite['results'] = sub_dict
    
    if method == 'mpfit':
        try:
            dictowrite.update({'mpfit stats': {'chi2r': float(params[method]['chi2r']), 'dof': float(params[method]['dof'])}})
        except KeyError:
            logger.debug("keyError: Key 'mpfit' not found in the results' dictionary")
            pass
    elif method == 'multinest':
        try:
            dictowrite.update({'multinest': {'log likelihood': params[method]['log likelihood']}})
        except KeyError:
            logger.debug("keyError: Key 'multinest' not found in the results' dictionary")
            pass
    #elif method == 'emcee':
        #try:
            #dictowrite.update({'emcee': {'xxx': params[method]['log likelihood']}})
        #except KeyError:
            #logger.debug("keyError: Key 'emcee' not found in the results' dictionary")
            #pass

    yaml.dump(dictowrite, outstream, default_flow_style=False)
    outstream.close()
