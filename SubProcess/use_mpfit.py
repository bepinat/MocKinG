import sys
import os
import time
import logging
import Tools.cap_mpfit as mpfit
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname('../Class/'), '..')))

logger = logging.getLogger('__mocking__')


def use_mpfit(model, params, quiet=False, gtol=1e-10, ftol=1e-10, xtol=1e-10):
    """
    Function call mpfit to perform the fit of the model

    :param Model2D model: model initialised
    :param dict params: dictionary which contains all parameters with the limits and if they are fixed or not
    :param Bool quiet: print or not verbose from the fit method
    :param float gtol: orthogonality desired between the function vector and the columns of the jacobian.
    :param float ftol: relative error desired in the sum of squares.
    :param float xtol: relative error desired in the approximate solution.
    """

    if quiet is True:
        verbose = 0
    else:
        verbose = 1
    
    # create the array of dictionary 'parinfo' needed by mpfit
    try:
        p0 = [params[compn][parn] for (compn, parn) in zip(model.model_compname, model.model_parname)]
        logger.info(' Parameters name are consistent.')
    except:
        logger.info(' Parameters name inconsistency. Check input parameters names in input file.')
        logger.info(' Parameters names should be')
        logger.info(print([(compn, modn, parn) for (modn, parn, compn) in zip(model.model_modname, model.model_parname, model.model_compname)]))
        return
    
    parinfo = [{'value': 0., 'fixed': 0, 'limited': [1, 1], 'limits': [0., 0.], 'modname': 0., 'parname': 0., 'step': 0.} for i in range(len(p0))]

    for i in range(len(p0)):
        modn = model.model_modname[i]
        parn = model.model_parname[i]
        compn = model.model_compname[i]
        
        parinfo[i]['value'] = p0[i]['value']
        parinfo[i]['parname'] = parn
        parinfo[i]['modname'] = modn
        parinfo[i]['compname'] = compn
        parinfo[i]['limits'] = params[compn][parn]['limits']
        if params[compn][parn]['fixed'] is None:
            params[compn][parn]['fixed'] = 0
        if params[compn][parn]['fixed'] == 1:
            parinfo[i]['fixed'] = 1

    logger.info(' start fit with mpfit')
    t1 = time.time()

    # ###  Call mpfit
    model_fit = mpfit.mpfit(model.least_square, parinfo=parinfo, autoderivative=1, gtol=gtol, ftol=ftol, xtol=xtol, quiet=verbose)
    # ###

    t2 = time.time()
    logger.info(' fit done in: {:6.2f} s\n'.format(t2-t1))

    # Print results on the prompt screen
    logger.info(' fit status: {}'.format(model_fit.status))
    logger.info(' Chi2R: {} DOF: {}'.format(model_fit.fnorm/model_fit.dof, model_fit.dof))

    message = ' '
    for par in model.model_parname:
        message += '{0:^{width}}'.format(par, width=12)
    logger.info(message)
    logger.info('-' * (len(model.model_parname)*12))
    
    message = ' '
    for par in model_fit.params:
        message += '{0:^{width}.{prec}}'.format(par, width=12, prec=6)
    logger.info(message)
    
    message = ' '
    for par in model_fit.perror:
        message += '{0:^{width}.{prec}}'.format(par, width=12, prec=6)
    logger.info(message)
    
    result = []
    for i in range(len(model.model_parname)):
        result.append({model.model_parname[i]: {'value': model_fit.params[i], 'error': model_fit.perror[i],
                                                'component': p0[i]['component'], 'model':model.model_modname[i],
                                                'desc': p0[i]['desc']}})

    return {'results': result,
            'mpfit': {'chi2r': model_fit.fnorm/model_fit.dof, 'dof': model_fit.dof}}
