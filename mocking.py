#!/usr/bin/env python
import argparse
import os
import sys
import logging
import numpy as np
import yaml
from astropy.io import fits

from Models.velocity_model import combined_velocity_model
import Tools.tools as tools
from Class.Images import Image
from Class.Model2D import Model2D
from Class.PSF import PSF
from SubProcess.use_mpfit import use_mpfit
from SubProcess.use_multinest import use_multinest

# Test if mpi4py is installed and if is it used
try:
    from mpi4py import MPI
    if MPI.COMM_WORLD.Get_size() > 0:
        rank = MPI.COMM_WORLD.Get_rank()
except ImportError:
    rank = 0
    pass

# For debug use logging.DEBUG instead of logging.INFO
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('__mocking__')


def mocking(path=None, filename=None, rank=0):
    """
    MockinG (Mocking/Modeling Kinematics of Galaxies) fits a model to a velocity field of galaxies.
    For more information see the help or refer to the git repository:
    
        https://gitlab.lam.fr/bepinat/mocking

    developed on python 3.6

    This program can be launched from console:
    
        mocking.py (path) (filename)

    or in a python shell/console by importing this file and call:

        mocking.mocking(path, filename, rank)

    To run with mpi type in the prompt:

        mpiexec -n (nbcore) mocking.py (path) (filename)

    Outputs are written in the directory where the YAML file is,
        the directory name is:  (name)_(method)_(obji)_(paramglobfix)_(compi)_(paramfixi)

    :param str path: path where the YAML configuration file is and where outputs will be written
    :param str filename: name of the YAML file which contains all parameters
    :param int rank: id of the thread when the program is run with MPI4PY
    """

    if rank == 0:
        try:
            logger.info(' entering in directory: {}'.format(path.split('/')[-2]))
        except IndexError as I:
            logger.info(' entering in directory: {}'.format(os.getcwd()))

    # Open the YAML file and preset some parts
    input_stream = open(tools.search_file(path, filename), 'r')
    config0 = yaml.safe_load(input_stream)
    input_stream.close()
    config = tools.test_config(config0)
    
    # Reading generic entries in the config file
    name = config['name']
    filesg = config['files']
    objects = config['objects']
    resparams = config['resol params']
    conffit = config['config fit']
    
    # Reading and organising object entries from the config file
    modparamsobj = tools.set_params(objects)  # the name of component, model and objects are specified in this dictionary
    files = tools.set_files(filesg, objects)  # for the 'flux' entry, one dictionary per object
    modelsobj = tools.set_models(objects)  # the name of component, model and objects are specified in this dictionary
    
    # Search and open images file
    vel = Image(tools.search_file(path, files['vel']))
    errvel = Image(tools.search_file(path, files['errvel']))
    if files['disp'] is not None:
        disp = Image(tools.search_file(path, files['disp']))
    fluxobj = {}
    for objn in files['flux']:
        fluxobj[objn] = Image(tools.search_file(path, files['flux'][objn]))
    
    # Case with one single object to start
    obj = list(modparamsobj.keys())[0]
    logger.warning(" Only the case for one object is implemented. Using object: {}".format(obj))
    modparams = modparamsobj[obj]
    flux = fluxobj[obj]
    models = modelsobj['objects'][obj]
    components = modelsobj['components'][obj]
    disp_info = config['objects'][obj]['dispersion info']
    
    ###################################################################################################
    # Test the size of the flux image with the velocity field and perform an interpolation if needed
    length_rap = flux.length / vel.get_lenght()
    height_rap = flux.height / vel.get_height()
    if length_rap == 1 or height_rap == 1:
        if length_rap == height_rap:
            if rank == 0:
                logger.info(" Oversampling set from config file: {}".format(resparams['oversample']))
            # XXX Should the oversampling be adjusted during the fit itself in case rt change? CF IDL code
            # XXX How to deal with oversampling if the model changes (and rt potentially does not exist anymore)?
            #flux.set_oversamp(int(np.ceil(8 / modparams['rt']['value'])))  # Case2: oversampling set from rt parameter: 
            # XXX Might be better to fix oversampling in the configuration file or default value
            flux.set_oversamp(int(resparams['oversample']))  # Case1: oversampling is set in the configuration
            if int(resparams['oversample']) != 1:
                flux.interpolation()
            whd = '_lores'
    else:
        if rank == 0:
            logger.warning(" Change the calculation of the ratio of oversampling"
                        "\n\t\t  pixel size is not in the fits header")
            logger.debug(" oversampling determined from size of images")
            logger.debug(" length: {} \theight: {}".format(length_rap, height_rap))
        flux.set_oversamp(int(length_rap))
        whd = '_hires'
    
    ####################################################################################################
    # Check psf file existence and import it or create a Gaussian or Moffat
    img_psf = None
    if files['psf'] is not None:
        img_psf = fits.getdata(tools.search_file(path, files['psf']))
        logger.debug('import psf from {}'.format(tools.search_file(path, files['psf'])))
    else:
        if resparams['beta'] is None:
            logger.debug('2D gaussian with fwhm={} pixels in low resolution'.format(np.sqrt(resparams['psfx']**2 + resparams['smooth']**2)))
            #logger.debug('2D gaussian with fwhm={} pixels in high resolution'.format(np.sqrt(resparams['psfx']**2 + resparams['smooth']**2) * vel.oversample))
        else:
            logger.debug('2D moffat with fwhm={} pixels in low resolution and beta={}, with an additional smoothing of fwhm={} pixels in low resolution'.format(resparams['psfx'], resparams['beta'], resparams['smooth']))
        # XXX gérer le cas où le paramètre beta est donné en input -> modifs faites dans tools_dev.py (test_config) -> A TESTER TBD
        # XXX ne devrait-on pas gérer le cas où psf n'est pas donné dans le config file 'section files' pour mettre par défaut None? -> modifs faites dans tools_dev.py (set_files) -> A TESTER  TBD
    psf = PSF(flux, img_psf=img_psf, fwhm_lr=resparams['psfx'], beta=resparams['beta'], smooth=resparams['smooth'])

    # Do the convolution and the rebinning of the high resolution flux
    flux.conv_inter_flux(psf)
    
    ####################################################################################################
    # Create the model from multiple components
    mod = combined_velocity_model(models, components)  # model definition

    # Initialise the model's class
    # XXX For each object and component, do a Model and generate a 'global model?'
    model = Model2D(vel, errvel, flux, psf, mod, psfz=resparams['psfz'], sig0=disp_info['sig'], slope=disp_info['slope'])
    
    # Check parameters name to avoid errors
    # XXX have to be done for each obj + component
    try:
        test = [modparams[compn][parn]['model'] for (compn, parn) in zip(model.model_compname, model.model_parname)]
        #print(test)
        logger.info(' Parameters name are consistent.')
    except:
        logger.info(' Parameters name inconsistency. Check input parameters names in input file.')
        logger.info(' Parameters names should be')
        logger.info(print([(compn, modn, parn) for (modn, parn, compn) in zip(model.model_modname, model.model_parname, model.model_compname)]))
        return

    # Create output path
    out_path = tools.make_dir(path, config)

    # Choose the method from the config file
    if conffit['method'] == 'mpfit' and rank == 0:
        conf_mpfit = conffit['mpfit']
        # Type conversion is needed because yaml parsing is incorrect with scientific notation...
        ftol = float(conf_mpfit['ftol'])
        gtol = float(conf_mpfit['gtol'])
        xtol = float(conf_mpfit['xtol'])

        results = use_mpfit(model, modparams, quiet=conffit['verbose'], ftol=ftol, gtol=gtol, xtol=xtol)
    elif conffit['method'] == 'multinest':
        conf_pymulti = conffit['multinest']
        # Type conversion is needed because yaml parsing is incorrect with scientific notation...
        pltstats = conf_pymulti['pltstats']
        max_iter = int(conf_pymulti['max_iter'])
        n_live_points = int(conf_pymulti['n_live_points'])
        sampling_efficiency = float(conf_pymulti['sampling_efficiency'])
        evidence_tolerance = float(conf_pymulti['evidence_tolerance'])
        n_iter_before_update = int(conf_pymulti['n_iter_before_update'])
        null_log_evidence = float(conf_pymulti['null_log_evidence'])
        max_modes = int(conf_pymulti['max_modes'])
        mode_tolerance = float(conf_pymulti['mode_tolerance'])
        
        results = use_multinest(model, modparams, quiet=conffit['verbose'], rank=rank, path=out_path, whd=whd,
                                max_iter=max_iter, pltstats=pltstats, n_live_points=n_live_points,
                                sampling_efficiency=sampling_efficiency, evidence_tolerance=evidence_tolerance,
                                n_iter_before_update=n_iter_before_update, null_log_evidence=null_log_evidence,
                                max_modes=max_modes, mode_tolerance=mode_tolerance)
    #elif conffit['method'] == 'emcee':
        #conf_emcee = conffit['emcee']
        #results = use_emcee(model, modparams, quiet=conffit['verbose'], nbp=conf_emcee['nbp'], pltstats=conf_emcee['plt stats'],
                            #rank=rank, path=out_path, whd=whd)
    else:
        if rank == 0:
            logger.error(" wrong fit's method input in the YAML config file"
                         " use 'mpfit' for MPFIT or 'multinest' for MultiNest")
            #logger.error(" wrong fit's method input in the YAML config file"
            #              " use 'mpfit' for MPFIT, 'multinest' for MultiNest or 'emcee' for EMCEE (MCMC)")
        else:
            logger.warning(" thread {} not used, for mpfit mpi is not necessary".format(rank))
        sys.exit()

    # Write fits file from the bestfit set of parameters and results in a YAML file
    if rank == 0:
        # Generation of maps with best fit parameters
        best_param = []
        for el in results['results']:
            for par in el:
                best_param.append(el[par]['value'])
                #print(par, el[par])
        #print(best_param)
        
        model.set_parameters(*best_param)
        model.velocity_map()
        model.vel_disp_map()

        tools.write_yaml(out_path, results, config['name'], method=conffit['method'], whd=whd)

        tools.write_fits(model.vel_map, out_path+'/modv'+whd, config, model, results, mask=vel.mask)

        tools.write_fits(vel.data-model.vel_map, out_path+'/resv'+whd, config, model, results, mask=vel.mask)

        tools.write_fits(model.vel_map_hd, out_path+'/modv_hd'+whd, config, model, results)

        tools.write_fits(model.vel_disp, out_path+'/modd'+whd, config, model, results, mask=vel.mask)

        if files['disp'] is not None:
            # XXX VERIFIER, pas sûr que ça fasse ce qu'il faudrait
            #tools.write_fits(*results['params'], resparams['sig'], model, disp.data - model.vel_disp, out_path+'/resd'+whd, mask=vel.mask)
            #tools.write_fits(np.sqrt(disp.data ** 2 - model.vel_disp ** 2), out_path+'/resd'+whd, config, model, results, mask=vel.mask)
            cok = disp.data >= model.vel_disp
            dispres = disp.data * 0
            dispres[cok] = np.sqrt(disp.data[cok] ** 2 - model.vel_disp[cok] ** 2)
            tools.write_fits(dispres, out_path+'/resd'+whd, config, model, results, mask=vel.mask)


if __name__ == '__main__':
    parser = argparse.ArgumentParser('-h', '--help', description=mocking.__doc__,
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('path', help="path where the YAML configuration file is and where outputs will be written")
    parser.add_argument('filename', help="name of the YAML file which contains all parameters")

    args = parser.parse_args()

    mocking(path=args.path, filename=args.filename, rank=rank)
