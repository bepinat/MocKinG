import math
import numpy as np
from numpy.fft import fftshift, rfft2, irfft2
#from numpy.fft import fftshift, fft2, ifft2
from Class.Images import Image
#import matplotlib.pyplot as plt


class PSF:
    """
    Create a 2D gaussian as PSF, store all the information about the psf.
    
    Perform the convolution of an image with the psf stored by the method 'convolution'.
    You can initiate this class with a fits file of a PSF, in this case no gaussian will be created.
    
    To avoid (at least minimize) any border effect, class PSF need information
        about the size of the highest resolution image passed by a Image's class.
    """

    def __init__(self, flux, img_psf=None, fwhm_lr=3.5, beta=None, smooth=0):
        """
            constructor of the PSF class

        :param Image flux: flux image, high resolution
        :param ndarray img_psf: image of the PSF
        :param float fwhm_lr: fwhm of a gaussian / moffat for the original (low) resolution, default is 3.5 pixels
        :param float beta: beta parameter of a moffat, default is None -> Gaussian
        """

        self.psf = img_psf
        self.beta = beta
        self.smooth = smooth * flux.oversample
        sigma_s = self.smooth / (2 * math.sqrt(2 * math.log(2)))

        if self.psf is None:
            self.im_size = flux.size
            self.fwhm = fwhm_lr * flux.oversample
            self.fwhm_f = np.sqrt(self.fwhm**2 + self.smooth**2)
            # (2*fwhm) is considered to avoid the boundary effect after the convolution
            self.size = np.array([2 * self.fwhm_f, 2 * self.fwhm_f])
            ideal_size = 2 ** np.ceil(np.log(self.im_size + 2 * self.size) / math.log(2))
            # to prevent any problems in the future, self.size has been forced to be an array of int
            self.size = np.array((ideal_size - self.im_size) / 2, dtype=int)

            y, x = np.indices((self.im_size[0] + 2 * self.size[0], self.im_size[1] + 2 * self.size[1]))
            
            if self.beta is None:
                sigma_o = self.fwhm / (2 * math.sqrt(2 * math.log(2)))
                self.psf_o = (1. / (2 * math.pi * sigma_o ** 2)) * np.exp(-((x - self.im_size[1] / 2 - self.size[1]) ** 2 + (y - self.im_size[0] / 2 - self.size[0]) ** 2) / (2.0 * sigma_o ** 2))
            else:
                alpha = self.fwhm / (2 * np.sqrt(2. ** (1 / self.beta) - 1))
                self.psf_o = (self.beta - 1) / (math.pi * alpha ** 2) * (1 + ((x - self.im_size[1] / 2 - self.size[1]) ** 2 + (y - self.im_size[0] / 2 - self.size[0]) ** 2) / alpha ** 2) ** (-beta)  # normalized in flux
            
        else:
            self.im_size = np.array(img_psf.shape, dtype=int)
            self.fwhm =  self.im_size[0] / 8  # could be 4 or 2...
            self.fwhm_f = int(np.ceil(np.sqrt(self.fwhm**2 + self.smooth**2)))
            self.size = np.array([2 * self.fwhm_f, 2 * self.fwhm_f], dtype=int)
            ideal_size = 2 ** np.ceil(np.log(self.im_size + 2 * self.size) / math.log(2))
            self.size = np.array((ideal_size - self.im_size) / 2, dtype=int)
            y, x = np.indices((self.im_size[0] + 2 * self.size[0], self.im_size[1] + 2 * self.size[1]))
            self.psf_o = np.zeros((self.im_size[0] + 2 * self.size[0], self.im_size[1] + 2 * self.size[1]))
            self.psf_o[int(self.size[0]):int(self.size[0] + self.im_size[0]), int(self.size[1]):int(self.size[1] + self.im_size[1])] = img_psf

        if sigma_s != 0:
            self.psf_s = (1. / (2 * math.pi * sigma_s ** 2)) * np.exp(-((x - self.im_size[1] / 2 - self.size[1]) ** 2 + (y - self.im_size[0] / 2 - self.size[0]) ** 2) / (2.0 * sigma_s ** 2))
            self.psf_s /= self.psf_s.sum()
            self.psf_s_fft2 = rfft2(fftshift(self.psf_s))
        else:
            self.psf_s = np.zeros((self.im_size[0] + 2 * self.size[0], self.im_size[1] + 2 * self.size[1]))
            self.psf_s[0, 0] = 1
            self.psf_s_fft2 = rfft2(self.psf_s)

        # normalization in order to ensure flux conservation
        self.psf_o /= self.psf_o.sum()
        self.psf_o_fft2 = rfft2(fftshift(self.psf_o))

        # accounting for Gaussian smoothing
        self.psf_fft2 = self.psf_o_fft2 * self.psf_s_fft2 
        self.psf = fftshift(irfft2(self.psf_fft2))
        self.psf_fft2_cube = rfft2(fftshift(self.psf).reshape(1, self.psf.shape[0], self.psf.shape[1]))


    def convolution(self, data):
        """
        Do the convolution product between the data and the PSF

        :param ndarray data:
        :return ndarray:
        """

        data2 = np.zeros((data.shape[0] + 2 * self.size[0], data.shape[1] + 2 * self.size[1]))
        data2[self.size[0]:data.shape[0] + self.size[0], self.size[1]:data.shape[1] + self.size[1]] = data

        data_conv = irfft2(rfft2(data2) * self.psf_fft2)
        data_conv = data_conv[self.size[0]:data.shape[0] + self.size[0], self.size[1]:data.shape[1] + self.size[1]]

        return data_conv


    def convolution_cube(self, cube):
        """
        Do the convolution product between the datacube and the PSF

        :param ndarray cube: data cube
        :return ndarray:
        """

        cube2 = np.zeros((cube.shape[0], cube.shape[1] + 2 * self.size[0], cube.shape[2] + 2 * self.size[1]))
        cube2[:,self.size[0]:cube.shape[1] + self.size[0], self.size[1]:cube.shape[2] + self.size[1]] = cube

        cube_conv = irfft2(rfft2(cube2) * self.psf_fft2_cube)
        cube_conv = cube_conv[:,self.size[0]:cube.shape[1] + self.size[0], self.size[1]:cube.shape[2] + self.size[1]]

        return cube_conv
