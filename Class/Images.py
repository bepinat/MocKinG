import numpy as np
from astropy.io import fits
# from scipy.interpolate import interp2d
from scipy.interpolate import RectBivariateSpline
import Tools.tools as tools


class Image:
    """
    Stocks a fits file and determines all parameters like size etc ...

    :param string or ndarray filename: path+name of the file, can be a numpy array.
    """
    def __init__(self, filename, mask=None):
        self.data_rebin = None

        if type(filename) is str:
            self.header = fits.getheader(filename)
            self.data = fits.getdata(filename)
            self.length = self.header['NAXIS1']
            self.height = self.header['NAXIS2']
            self.size = np.array(np.shape(self.data))
            self.len = self.length*self.height
            self.oversample = 1
            if mask is None:
                self.mask = np.logical_not(np.isnan(self.data))
            else:
                self.mask = mask
            self.nan_to_num()

        # Add for create model without Image's class
        if type(filename) is np.ndarray:
            self.header = None
            self.data = np.nan_to_num(filename)
            self.size = np.array(np.shape(self.data))
            self.height = self.size[0]
            self.length = self.size[1]
            self.len = self.length*self.height
            self.oversample = 1
            if mask is None:
                self.mask = np.logical_not(np.isnan(self.data))
            else:
                self.mask = mask
            self.nan_to_num()

    def nan_to_num(self):
        """
        Converts 'nan' into 0
        """
        self.data = np.nan_to_num(self.data)

    def get_size(self):
        return self.size

    def get_lenght(self):
        return self.length

    def get_height(self):
        return self.height

    def set_oversamp(self, oversamp):
        self.oversample = oversamp

    def get_oversamp(self):
        return self.oversample

    def interpolation(self):
        """
        Performs an interpolation of the image at higher resolution and stock the new image
        """
        x = np.arange(self.length) + 0.5
        y = np.arange(self.height) + 0.5
        new_x = (np.arange(self.length * self.oversample) + 0.5) / self.oversample
        new_y = (np.arange(self.height * self.oversample) + 0.5) / self.oversample
        
        #x = np.arange(self.length)  # + 0.5*self.oversample
        #y = np.arange(self.height)  # + 0.5*self.oversample
        #new_x = np.linspace(0, self.length, self.length * int(self.oversample))
        #new_y = np.linspace(0, self.height, self.height * int(self.oversample))
        
        #x = np.linspace(0, self.length, self.length)  # + 0.5*self.oversample
        #y = np.linspace(0, self.height, self.height)  # + 0.5*self.oversample
        #new_x = np.linspace(0, self.length, self.length * int(self.oversample))
        #new_y = np.linspace(0, self.height, self.height * int(self.oversample))
        
        # func = interp2d(x, y, self.data, kind='linear', fill_value=0)
        # self.data = np.array(func(new_x, new_y))

        func = RectBivariateSpline(y, x, self.data, kx=1, ky=1)
        self.data = np.array(func(new_y, new_x))
        
        self.height *= self.oversample
        self.length *= self.oversample
        self.size *= self.oversample

    def conv_inter_flux(self, psf):
        """
        Does a convolution of the interpolated image with a psf and stocks the result

        :param PSF psf: psf object
        """
        self.data_rebin = tools.rebin_data(psf.convolution(self.data), self.oversample)
