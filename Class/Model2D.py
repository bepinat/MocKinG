import math
import numpy as np
import Tools.tools as tools
from scipy import constants as ct
from Class.PSF import PSF
from Class.Images import Image


class Model2D:
    """
    Models in 2D of the velocity field of galaxies
    Compute the velocity field and the dispersion of a model using observed data in input.
    This model can be used with any rotational curve models (in 'velocity_model.py').
    """
    def __init__(self, vel, errvel, flux, psf, vel_model, psfz=0, sig0=0, slope=0.):
        """

        :param Image vel: represent the velocity field to fit
        :param Image errvel: the error map of the velocity (vel)
        :param Image flux: flux distribution at the same or in high resolution than the velocity
        :param PSF psf: the class of the psf
        :param func vel_model: velocity function used to create the model
        :param float psfz: velocity dispersion corresponding to the spectral resolution
        :param float sig0: velocity dispersion of the model
        :param float slope: slope of the velocity dispersion
        """
        
        # Parameters which must be initialized
        #self.vel_model = vel_model
        self.vel_model = vel_model.vel_model
        self.model_sig0 = sig0
        self.model_slope = slope
        self.psfz = psfz
        self.vel_map = np.zeros(vel.get_size())
        self.vel_disp = np.zeros(vel.get_size())
        self.vel_map_hd = np.zeros(np.array(flux.get_size()))
        self.vel = vel
        self.errvel = errvel
        self.flux = flux
        self.psf = psf
        self.oversamp = self.flux.get_oversamp()
        self.indices = tools.lowres_indices_highres_frame(flux.get_size(), self.oversamp)

        # We initiate the geometrical model parameters
        self.model_pa = None
        self.model_inc = None
        self.model_xc = None
        self.model_yc = None
        self.model_vs = None
        
        # We initiate the associated coordinate maps in the galaxy frame
        self.model_radius = None
        self.model_theta = None
        
        # We write an ordered list containing the names of the model parameters
        self.model_parname = ['xc', 'yc', 'pa', 'inc', 'vs']
        self.model_modname = ['geometrical', 'geometrical', 'geometrical', 'geometrical', 'geometrical']
        self.model_compname = ['geometrical', 'geometrical', 'geometrical', 'geometrical', 'geometrical']
        # We initiate a dictionary containing the names of rotation curve parameters. The number of arguments adjusts automatically
        #ii = 0
        for name in vel_model.model_parname:
            self.model_parname.append(name)
        for name in vel_model.model_modname:
            self.model_modname.append(name)
        for name in vel_model.model_compname:
            self.model_compname.append(name)
            #ii += 1
        #print(self.model_parname)
        #for name in self.vel_model.__code__.co_varnames[1:self.vel_model.__code__.co_argcount]:
            #self.model_parname.append(name)
        #self.model_RCparams_names = self.vel_model.__code__.co_varnames[1:self.vel_model.__code__.co_argcount]  # can be useful to check the parameters are provided with the correct order
        #self.model_RCparams_values = [None for par in self.model_RCparams_names]
        #self.model_RCparams = dict((par, None) for par in self.vel_model.__code__.co_varnames[1:self.vel_model.__code__.co_argcount])
        self.model_RCparams = None


    def set_parameters(self, xc, yc, pa, inc, vs, *args):
        """
        Sets the value of the model parameters

        :param float xc: abscissa of the center in pixel
        :param float yc: ordinate of the center in pixel
        :param float pa: position angle of the major axis in degree
        :param float inc: inclination of the disk in degree
        :param float vs: systemic velocity in km/s
        :param list of float args: list that contains other model arguments
        """

        self.model_pa = pa
        self.model_inc = inc
        self.model_xc = xc
        self.model_yc = yc
        self.model_vs = vs
        #self.model_radius, self.model_theta = tools.sky_coord_to_galactic(self.model_xc, self.model_yc, self.model_pa, self.model_inc, im_size=np.shape(self.vel_map_hd), oversamp=self.oversamp)
        self.model_radius, self.model_theta = tools.sky_coord_to_galactic(self.indices[1], self.indices[0], self.model_xc, self.model_yc, self.model_pa, self.model_inc)
        self.model_RCparams = args


    def get_parameter(self):
        """
        Gets the actual parameters of the model (in low resolution scale)

        :return ndarray:
        """
        return [self.model_xc, self.model_yc, self.model_pa, self.model_inc, self.model_vs, *self.model_RCparams]


    def disk_velocity(self):
        """
        Computes the velocity field

        :return ndarray:
        """

        vr = self.vel_model(self.model_radius, *self.model_RCparams)

        # Calculation of the velocity field
        v = vr * math.sin(math.radians(self.model_inc)) * self.model_theta + self.model_vs
        return v


    def velocity_map(self):
        """
        Computes the velocity map of the model
        """

        self.vel_map_hd = self.disk_velocity()
        vel_times_flux = tools.rebin_data(self.psf.convolution(self.flux.data * self.vel_map_hd), self.oversamp)
        self.vel_map[self.vel.mask] = vel_times_flux[self.vel.mask] / self.flux.data_rebin[self.vel.mask]


    def linear_velocity_dispersion(self):
        """
        Returns the velocity dispersion map needed for the fit process

        :return ndarray: velocity dispersion map
        """
        # Calculation of the velocity dispersion
        sig = self.model_sig0 + self.model_slope * np.abs(self.model_radius)
        sig[np.where(sig <= 0)] = 0

        return sig


    def vel_disp_map(self):
        """
        Computes the velocity dispersion map from the velocity field (with bestfit parameters) and the flux distribution

        :return ndarray: velocity dispersion map
        """

        term2 = np.zeros(self.vel.size)
        term3 = np.zeros(self.vel.size)
        
        term2[self.vel.mask] = tools.rebin_data(self.psf.convolution(self.vel_map_hd ** 2 * self.flux.data), self.flux.oversample)[self.vel.mask] \
                               / self.flux.data_rebin[self.vel.mask]
        term3[self.vel.mask] = (tools.rebin_data(self.psf.convolution(self.vel_map_hd * self.flux.data), self.flux.oversample)[self.vel.mask] /
                                self.flux.data_rebin[self.vel.mask]) ** 2

        self.vel_disp = np.sqrt(term2 - term3 + self.psfz**2)


    def vel_disp_map_model(self):
        """
        Computes the velocity dispersion map from the velocity field (with bestfit parameters) and the flux distribution

        :return ndarray: velocity dispersion map
        """

        term1 = np.zeros(self.vel.size)
        term2 = np.zeros(self.vel.size)
        term3 = np.zeros(self.vel.size)

        sig = self.linear_velocity_dispersion()

        term1[self.vel.mask] = tools.rebin_data(self.psf.convolution(sig ** 2 * self.flux.data), self.flux.oversample)[self.vel.mask] \
                               / self.flux.data_rebin[self.vel.mask]
        term2[self.vel.mask] = tools.rebin_data(self.psf.convolution(self.vel_map_hd ** 2 * self.flux.data), self.flux.oversample)[self.vel.mask] \
                               / self.flux.data_rebin[self.vel.mask]
        term3[self.vel.mask] = (tools.rebin_data(self.psf.convolution(self.vel_map_hd * self.flux.data), self.flux.oversample)[self.vel.mask] /
                                self.flux.data_rebin[self.vel.mask]) ** 2

        self.vel_disp = np.sqrt(term1 + term2 - term3 + self.psfz**2)


    def least_square(self, p, fjac=None):
        """
        Function minimized by mpfit.

        Returns (data-model)^2/err^2

        :param ndarray p: array of parameters
        :return ndarray:
        """

        self.set_parameters(*p)
        self.velocity_map()

        return [0, np.reshape((self.vel.data[self.vel.mask]-self.vel_map[self.vel.mask])/self.errvel.data[self.vel.mask], -1)]


    def log_likelihood(self, cube, ndim, nparams):
        """
        Log likelihood function which maximized by multinest

        Returns -sum[(data-model)^2/(2*err^2)]

        :param ndarray cube: data whith n_params dimension
        :param int ndim: number of dimension if different of the number of paramerters
        :param int nparams: number of parameters
        :return float:
        """
        
        #self.set_parameters(cube[0], cube[1], cube[2], cube[3], cube[4], cube[5], cube[6])
        self.set_parameters(cube[0], cube[1], cube[2], cube[3], cube[4], *cube[5:ndim])
        self.velocity_map()

        chi2 = -(self.vel_map[self.vel.mask] - self.vel.data[self.vel.mask])**2 / (2*self.errvel.data[self.vel.mask]**2)

        return np.sum(chi2)

