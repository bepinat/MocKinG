import numpy as np
from scipy.special import i0, i1, k0, k1


"""
Library which contains elementary rotation curve models and which allows to combine them quadratically
"""


class combined_velocity_model:
    '''
    Class which allows to combine quadratically velocity models. This class has attributes useful for handling the model parameters.
    '''
    def __init__(self, list_name, list_comp):
        '''
        :param list list_name: list which contains the names of the models to combine
        '''
        self.list_name = list_name
        self.model_parname = []
        self.model_modname = []
        self.model_compname = []
        self.final_func_name = ''
        for (model, component) in zip(list_name, list_comp):
            self.final_func_name += model + '+'
            func = list_model[model]
            func_parname = []
            for name in func.__code__.co_varnames[1:func.__code__.co_argcount]:
                self.model_parname.append(name)
                self.model_modname.append(model)
                self.model_compname.append(component)
                #func_parname.append(name)
            #self.model_parname.append([func, func_parname])
            #self.model_parname.append(func_parname)
        self.final_func_name = self.final_func_name[:-1]
    
    def vel_model(self, r, *args):
        '''
        Final combined model
        
        :param ndarray r: 1D or 2D array which contains the radius
        :param list of float args: list that contains model arguments
        :return ndarray: 1D or 2D array with values of the model at the specified radius
        '''
        n_params_prev = 0
        v2 = np.zeros(r.shape)
        for model in self.list_name:
            func = list_model[model]
            n_params = n_params_prev + func.__code__.co_argcount - 1
            params = args[n_params_prev:n_params]
            #print(model, n_params_prev, n_params, params)
            # Ajouter une verification des paramtres d'entree
            #print(params)
            v2 += func(r, *params) ** 2
            n_params_prev = n_params
        return np.sqrt(v2)


def exponential_velocity(r, rt, vm):
    """
    Rotation curve for an exponential disk

    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: radius at which the maximum velocity is reached
    :param float vm: Maximum velocity of the model
    """

    rd = rt / 2.15        # disk scale length, maximum is reached at 2.15 rd
    vr = np.zeros(np.shape(r))
    q = np.where(r != 0)      # To prevent any problem in the center

    vr[q] = r[q] / rd * vm / 0.8798243 * np.sqrt(i0(0.5 * r[q] / rd) * k0(0.5 * r[q] / rd) - i1(0.5 * r[q] / rd) * k1(0.5 * r[q] / rd))

    return vr


def bovy_velocity(r, rd, vm_RT, vm_corr):
    """
    Approximate rotation curve for a double exponential disk with small thickness. See https://galaxiesbook.org/#mjx-eqn-eq-invhankel-surfdens-0 for a derivation from Bovy J.

    Note:
      Perhaps try to redefine with a maximum velocity + radius where this is reached -> seems not easily feasible

    :param ndarray r: 1D or 2D array which contains the radius
    :param float rd: disk scalelength
    :param float vm_RT: maximum velocity if the disk was razor-thin
    :param float vm_corr: maximum velocity of the correction (see Mercier et al., 2022)
    """

    # Position of maximum velocity for a razor-thin disk
    rt       = 2.15*rd

    vr       = np.zeros(np.shape(r))
    v_RT2    = exponential_velocity(r, rt, vm_RT)**2
    v_corr2  = vm_corr*vm_corr * r * np.exp(1-r/rd) / rd

    # Correction becomes too large near the centre, so we approximate by placing zero in those parts
    mask     = np.where(v_corr2 < v_RT2)
    vr[mask] = np.sqrt(v_RT2[mask] - v_corr2[mask])

    return vr

def bovy_velocity_corrected(r, rd, vm_RT, vm_corr, q0):
    """
    Approximate rotation curve for a double exponential disk with small thickness. See https://galaxiesbook.org/#mjx-eqn-eq-invhankel-surfdens-0 for a derivation from Bovy J.
    This approximation is corrected in the centre so that it reaches 0 at r=0 only (and not before as is the case with the Bovy approximation). The way it is corrected is that it becomes the tangent line passing through the point r=0 untill the line crosses the rotation curve, and the rotation curve further out.

    :param ndarray r: 1D or 2D array which contains the radius
    :param float rd: disk scalelength
    :param float vm_RT: maximum velocity if the disk was razor-thin
    :param float vm_corr: maximum velocity of the correction (see Mercier et al., 2022)
    :param float q0: intrinsic thickness
    """

    # Position of maximum velocity for a razor-thin disk
    rt        = 2.15*rd

    vr        = np.zeros(np.shape(r))
    v_RT2     = exponential_velocity(r, rt, vm_RT)**2
    v_corr2   = vm_corr*vm_corr * r * np.exp(1-r/rd) / rd

    # Compute the radius and slope for the linear approximation
    lq0       = np.log(q0)
    r_lin     = rd * np.exp(0.767 + 0.86*lq0 - 0.14*lq0**2 - 0.023*lq0**3 + 0.005*lq0**4 + 0.001*lq0**5)
    slope_lin = bovy_velocity(np.array([r_lin]), rd, vm_RT, vm_corr)[0] / r_lin

    # Compute the mask where the linear approximation applies
    mask      = r <= r_lin

    # Compute the velocity in the inner parts (linear approximation) and in the outer parts
    vr[mask]  = r[mask] * slope_lin
    vr[~mask] = np.sqrt(v_RT2[~mask] - v_corr2[~mask])

    return vr


def flat_velocity(r, rt, vm):
    """
    Flat rotation curve:
        v = r/rt * vm   when r<=rt
        v = vm          when r>=rt

    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: radius at which the maximum velocity is reached
    :param float vm: maximum velocity of the model
    """

    vr = np.zeros(np.shape(r))

    vr[np.where(r <= rt)] = vm*r[np.where(r <= rt)]/rt
    vr[np.where(r > rt)] = vm

    return vr


def arctan_velocity(r, rt, vm):
    """
    Arctangent rotation curve
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: maximum velocity of the model
    """

    return 2*vm/np.pi*np.arctan(2*r/rt)


def kent_velocity(r, rt, vm):
    """
    Rotation curve for a Kent or pseudo isothermal sphere density profile (e.g. Korsaga et al. 2018)
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: maximum velocity of the model
    """

    vr = np.zeros(np.shape(r))
    q = np.where(r != 0)      # To prevent any problem in the center
    vr[q] = vm * np.sqrt(1 - rt/r[q] * np.arctan(r[q]/rt))

    return vr


def hubblemodified_velocity(r, rt, vm):
    """
    Rotation curve for a Hubble modified density profile (see Binney & Tremaine)
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: maximum velocity of the model
    """

    rd = rt / 2.92    # scalelength, maximum is reached at 2.15 rd
    vr = np.zeros(np.shape(r))
    q = np.where(r != 0)      # To prevent any problem in the center

    vr[q] = vm / 0.53851 * np.sqrt(1 / (r[q]/rd) * (np.log(r[q]/rd + np.sqrt(1 + (r[q]/rd)**2)) - r[q]/rd * (1 + (r[q]/rd)**2)**(-0.5)))

    return vr


def nfw_velocity(r, rt, vm):
    """
    Rotation curve for a Navaro Frenk & White (NFW) density profile (see Binney & Tremaine)
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: maximum velocity of the model
    """

    rd = rt / 2.163    # scalelength, maximum is reached at 2.163 rd
    vr = np.zeros(np.shape(r))
    q = np.where(r != 0)      # To prevent any problem in the center

    vr[q] = vm / 0.46499096 * np.sqrt((np.log(1 + r[q]/rd) - (r[q]/rd) / (1 + r[q]/rd)) / (r[q]/rd))

    return vr


def iso_reduced_velocity(r):
    """
    Reduced rotation curve for a real isothermal profile
    
    :param ndarray: 1D array which contains the reduced radius
    """
    rho = np.ones(len(r))
    dlnrho = np.zeros(len(r))
    drho = np.zeros(len(r))
    for i in np.arange(1,len(r)):
        rho[i] = rho[i-1] + (r[i] - r[i-1]) * drho[i-1]
        dlnrho[i] = 1 / r[i]**2 * (r[i-1]**2 * dlnrho[i-1] - 9 * rho[i] * r[i]**2 * (r[i] - r[i-1]))
        drho[i] = rho[i-1] * dlnrho[i]

    derivlogrho = np.zeros(len(r))
    derivlogrho[2:] = (np.log(rho[2:]) - np.log(rho[1:-1])) / (np.log(r[2:]) - np.log(r[1:-1]))
    
    vc = np.sqrt(-derivlogrho)
    vc[1] = (vc[0] + vc[2])/2
    return rho, vc


nel = 100000
r_red = np.arange(nel)/200
rho_red, vc_red = iso_reduced_velocity(r_red)


def iso_velocity(r, rt, vm):
    """
    Rotation curve for a 'true' Isothermal sphere density profile (see Binney & Tremaine) with a core.
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: normalisation velocity of the model
    """
    vc = np.interp(r/rt, r_red, vc_red) * vm
    return vc


#def iso_velocity(r, rt, vm):
    #"""
    #Rotation curve for a Isothermal sphere density profile (see Binney & Tremaine)
    
    #XXX See function in mass model
    #XXX possibility to use spline by segment as done in the NEFER code for filter transmission shift computation
    
    #:param ndarray r: 1D or 2D array which contains the radius
    #:param float rt: scalelength
    #:param float vm: maximum velocity of the model
    #"""

    
def hernquist_velocity(r, rt, vm):
    """
    Rotation curve for a Hernquist density profile (see Binney & Tremaine)
    
    WARNING: analytical solution comes from my own computation. Could be WRONG !!!
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt:  scalelength of the Hernquist profile
    :param float vm:  maximum velocity of the model (=0.5* sqrt(G*M/rt))
    """
    
    return 2*vm*np.sqrt(rt*r)/(rt+r)

    
#def jaffe_velocity():
    #"""
    #Rotation curve for a Jaffe density profile (see Binney & Tremaine)
    
    #:param ndarray r: 1D or 2D array which contains the radius
    #:param float rt: scalelength
    #:param float vm: maximum velocity of the model
    #"""
    
    
#def einasto_velocity():
    #"""
    #Rotation curve for a Einasto density profile
    
    #:param ndarray r: 1D or 2D array which contains the radius
    #:param float rt: scalelength
    #:param float vm: maximum velocity of the model
    #:param float
    #"""
    
    
#def adiabatic_contraction():
    #"""
    #Should be applicable to any model and have to include baryon distribution
    #"""
    
#def sersic():
    
    
def courteau_velocity(r, rt, vm, a):
    """
    Courteau rotation curve
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vm: typical velocity
    :param float a: Courteau index
    """
    
    vr = np.zeros(np.shape(r))
    q = np.where(r != 0)      # To prevent any problem in the center

    vr[q] = vm / (1 + (1 / (r[q]/rt))**a) ** (1 / a)

    return vr


def zhao_velocity(r, rt, vt, a, b, g):
    """
    Zhao rotation curve (see Epinat et al. 2008)
    
    :param ndarray r: 1D or 2D array which contains the radius
    :param float rt: scalelength
    :param float vt: typical velocity
    :param float a: first Zhao index
    :param float b: second Zhao index, use b=0 to force b=a
    :param float g: third Zhao index
    """

    if (a == 0) | (b == 0):
        exp = 1
    else:
        exp = b/a


    return vt * (r/rt)**g / (1 + (r/rt)**a)**exp


# Must be at the end of the file
list_model = {'expo': exponential_velocity, 'flat': flat_velocity, 'atan': arctan_velocity, 'hmod': hubblemodified_velocity, 'nfw': nfw_velocity, 'kent': kent_velocity, 'iso': iso_velocity,
              'bovy': bovy_velocity, 'bovy_corr': bovy_velocity_corrected,
              'hernquist': hernquist_velocity, #'jaffe': jaffe_velocity, 'einasto': einasto_velocity,
              'courteau': courteau_velocity, 'zhao': zhao_velocity}
