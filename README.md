# MocKinG: Mocking/Modeling Kinematics of Galaxies

## What is it?
**MocKinG** fits a velocity model on an observed velocity field of galaxy. 

## Model

To compute the model, we use the method of line moments from:
[Epinat, B., Amram, P., Balkowski, C., & Marcelin, M. 2010, MNRAS, 401, 2113](http://adsabs.harvard.edu/abs/2010MNRAS.401.2113E)

But one can add more rotational curves in "velocity_model.py" or create another model like cube etc.

### There are two methods implemented

- MPFIT: which uses the Levenberg-Marquardt technique to solve the least-squares problem. **MocKinG** uses the python 3 implementation of **mpfit** of M. Cappellari, see [this link](https://www-astro.physics.ox.ac.uk/~mxc/software/)

- PyMultiNest : which uses a Nested Sampling Monte Carlo library and bayesian statistics.

## Installation

The program needs PyMultiNest to be installed, please refer to [this link](https://johannesbuchner.github.io/PyMultiNest/) for its installation guide.

The program also needs **astropy** and **yaml** libraries.

## How to launch the program?

**MocKinG** can be launched from prompt with
```
mocking.py path filename
```
or in a python script/console by importing first **MocKinG** and then calling the main function
```
import mocking.py

mocking.mocking(path, filename, rank=0)
```

where path can be absolute or relative, filename is the name of the **YAML** configuration file. The parameter rank is the id of the thread.
For example, if you run the program with mpi and 4 core, rank may be equal from 0 to 3.

### Compatible with MPI4PY

For more information and how to install it, follow [this link](http://pythonhosted.org/mpi4py/).
To execute the program with **mpi4py**:
```
mpiexec -n (nbcore) mocking.py path filename
```

## Output

Outputs are written in the directory where the **YAML** config file is. The name of the directory depends on the method, the model and fixed parameters like: 
method_model_fixedparams. A summary of the parameters of the model is written in the header of the fits file and in a **YAML** file. 

## Example

There is an Example directory in which you have fits files and some **YAML** file as examples to try the program:

`mocking.py Example/ input2_flat_mpfit.yaml`
`mocking.py Example/ input2_flat_expo_mpfit.yaml`

## Create model

The subprocess to create map was used to validate the program (cross test) with existing programs.

## Other features

- The toolbox enables to create input **YAML** files for a list of galaxies.

### Credits

Written by Jeremy Dumoulin under supervision of Benoît Epinat, maintained and new features by Benoît Epinat.
